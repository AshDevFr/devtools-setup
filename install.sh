#!/bin/sh
set -e

command_exists() {
  command -v "$@" >/dev/null 2>&1
}

error() {
  echo ${RED}"Error: $@"${RESET} >&2
}

underline() {
  echo "$(printf '\033[4m')$@$(printf '\033[24m')"
}

setup_color() {
  # Only use colors if connected to a terminal
  if [ -t 1 ]; then
    RED=$(printf '\033[31m')
    GREEN=$(printf '\033[32m')
    YELLOW=$(printf '\033[33m')
    BLUE=$(printf '\033[34m')
    BOLD=$(printf '\033[1m')
    RESET=$(printf '\033[m')
  else
    RED=""
    GREEN=""
    YELLOW=""
    BLUE=""
    BOLD=""
    RESET=""
  fi
}

install() {
  if ! command_exists git; then
    error "Git is required"
    exit 1
  fi

  if [ ! -d "$HOME/.tool-setup" ]; then
    git clone https://gitlab.com/AshDevFr/devtools-setup.git ~/.tool-setup
  else
    (cd ~/.tool-setup && git pull --rebase)
  fi

  cd ~/.tool-setup
  ./install-main.sh
}

setup_color
install
