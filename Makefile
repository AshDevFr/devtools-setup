TEST_IMAGE?=alpine
VERSION?=latest

INSTALLER_MOUNT=-v "$(CURDIR)/install.sh":/install.sh:ro
INSTALLER_MOUNT=-v "$(CURDIR)/install-main.sh":/install-main.sh:ro
CONFIGS_MOUNT=-v "$(CURDIR)/configs":/scripts/configs:ro
SCRIPTS_MOUNT=-v "$(CURDIR)/scripts":/scripts/scripts:ro
TESTS_MOUNT=-v "$(CURDIR)/specs":/scripts/specs:ro

.PHONY: test
test:
	docker run --rm \
		$(INSTALLER_MOUNT) \
		$(CONFIGS_MOUNT) \
		$(SCRIPTS_MOUNT) \
		$(TESTS_MOUNT) \
		-w /scripts \
		$(TEST_IMAGE):$(VERSION) \
		sh specs/tests-main.sh
