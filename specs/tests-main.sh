#!/bin/sh
set -e

. specs/helpers.sh

run_setup() {
  sh -c "/install-main.sh"
}

run_tests() {
  echo "# Running the tests"

  test_exisiting_command curl
  test_exisiting_command zsh
  test_exisiting_command tmux
  test_exisiting_command git

  ls -la ~/.oh-my-zsh
  test_exisiting_directory ~/.oh-my-zsh

  if command_exists nvim; then
    test_exisiting_file ~/.config/nvim/colors/molokai.vim
  fi

  test_exisiting_directory ~/.bin
  test_exisiting_file ~/.bin/tmux-dev

  ./specs/tests-zsh.sh
}

run_setup
setup_color

run_tests
