#!/bin/sh

setup_color() {
  RED=$(printf '\033[31m')
  GREEN=$(printf '\033[32m')
  RESET=$(printf '\033[m')
}

success() {
  echo ${GREEN}"Success: $@"${RESET} >&2
}

error() {
  echo ${RED}"Error: $@"${RESET} >&2
}

command_exists() {
  command -v "$@" >/dev/null 2>&1
}

test_exisiting_command() {
  if command_exists $1; then
    success "Command $1 exists"
  else
    error "Command $1 is missing"
    exit 1
  fi
}

test_exisiting_directory() {
  if [ -d "$1" ]; then
    success "Directory $1 exists"
  else
    error "Directory $1 is missing"
    exit 1
  fi
}

test_exisiting_file() {
  if [ -f "$1" ]; then
    success "File $1 exists"
  else
    error "File $1 is missing"
    exit 1
  fi
}