#!/bin/sh

command_exists() {
  command -v "$@" >/dev/null 2>&1
}

is_darwin() {
  case "$(uname -s)" in
  *darwin*) true ;;
  *Darwin*) true ;;
  *) false ;;
  esac
}

get_distribution() {
  lsb_dist=""
  # Every system that we officially support has /etc/os-release
  if [ -r /etc/os-release ]; then
    lsb_dist="$(. /etc/os-release && echo "$ID")"
  fi
  # Returning an empty string here should be alright since the
  # case statements don't act unless you provide an actual value
  echo "$lsb_dist"
}

set_lsb_dist() {
  # perform some very rudimentary platform detection
  lsb_dist=$(get_distribution)
  LSB_DIST="$(echo "$lsb_dist" | tr '[:upper:]' '[:lower:]')"
}

update_repo() {
  echo "# Updating the repo for $LSB_DIST"
  case "$LSB_DIST" in
  ubuntu | debian | raspbian)
    apt update -qq
    ;;

  centos | fedora | rhel)
    yum update
    ;;

  alpine)
    apk update
    ;;

  *)
    ;;
  esac
}

install_packages() {
  PACKAGES=""
  while [ $# -gt 0 ]; do
    if ! command_exists $1; then
      PACKAGES="$PACKAGES$1 "
    fi
    shift $(($# > 0 ? 1 : 0))
  done

  if [ -n "$PACKAGES" ]; then
    echo "# Installing $PACKAGES for $LSB_DIST"
    case "$LSB_DIST" in
    ubuntu | debian | raspbian)
      apt install -y $PACKAGES
      ;;

    centos | fedora | rhel)
      yum install -y $PACKAGES
      ;;

    alpine)
      apk add $PACKAGES
      ;;

    *)
      if [ -z "$LSB_DIST" ]; then
        if is_darwin; then
          brew install $PACKAGES
        fi
      fi
      ;;
    esac
  fi
}

set_lsb_dist
update_repo
install_packages curl git