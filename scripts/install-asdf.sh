#!/bin/bash
set -e

echo "# Installing ASDF"
# http://asdf-vm.com/guide/getting-started.html#_1-install-dependencies
if [ ! -d "$HOME/.asdf" ]; then
  git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.10.0
fi
echo "# Installing ASDF - Source"
source ~/.asdf/asdf.sh
echo "# Installing ASDF - Update"
asdf update --head

echo "# Installing ASDF - Add NodeJS plugin"
if asdf plugin list | grep -q "nodejs"; then
  echo "NodeJS Plugin alredy installed"
  asdf plugin update nodejs
else
  asdf plugin-add nodejs
fi

echo "# Installing ASDF - Install NodeJS"
echo "Latest NodeJS version available: $(asdf latest nodejs)"
asdf install nodejs 20.9.0
asdf global nodejs 20.9.0

echo "# Installing ASDF - Add Yarn plugin"
if asdf plugin list | grep -q "yarn"; then
  echo "Yarn Plugin alredy installed"
  asdf plugin update yarn
else
  asdf plugin-add yarn
fi

echo "# Installing ASDF - Install Yarn"
echo "Latest Yarn version available: $(asdf latest yarn)"
asdf install yarn latest
asdf global yarn latest

echo "# Installing ASDF - Add Golang plugin"
if asdf plugin list | grep -q "golang"; then
  echo "Golang Plugin alredy installed"
  asdf plugin update golang
else
  asdf plugin-add golang
fi

echo "# Installing ASDF - Install Golang"
echo "Latest Golang version available: $(asdf latest golang)"
asdf install golang latest
asdf global golang latest

asdf reshim