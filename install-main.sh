#!/bin/sh
set -e

DRY_RUN=${DRY_RUN:-}
while [ $# -gt 0 ]; do
  case "$1" in
  --options)
    options="$2"
    shift
    ;;
  --dry-run)
    DRY_RUN=1
    ;;
  --*)
    echo "Illegal option $1"
    ;;
  esac
  shift $(($# > 0 ? 1 : 0))
done

command_exists() {
  command -v "$@" >/dev/null 2>&1
}

error() {
  echo ${RED}"Error: $@"${RESET} >&2
}

underline() {
  echo "$(printf '\033[4m')$@$(printf '\033[24m')"
}

setup_color() {
  # Only use colors if connected to a terminal
  if [ -t 1 ]; then
    RED=$(printf '\033[31m')
    GREEN=$(printf '\033[32m')
    YELLOW=$(printf '\033[33m')
    BLUE=$(printf '\033[34m')
    BOLD=$(printf '\033[1m')
    RESET=$(printf '\033[m')
  else
    RED=""
    GREEN=""
    YELLOW=""
    BLUE=""
    BOLD=""
    RESET=""
  fi
}

is_dry_run() {
  if [ -z "$DRY_RUN" ]; then
    return 1
  else
    return 0
  fi
}

is_darwin() {
  case "$(uname -s)" in
  *darwin*) true ;;
  *Darwin*) true ;;
  *) false ;;
  esac
}

get_distribution() {
  lsb_dist=""
  # Every system that we officially support has /etc/os-release
  if [ -r /etc/os-release ]; then
    lsb_dist="$(. /etc/os-release && echo "$ID")"
  fi
  # Returning an empty string here should be alright since the
  # case statements don't act unless you provide an actual value
  echo "$lsb_dist"
}

set_lsb_dist() {
  # perform some very rudimentary platform detection
  lsb_dist=$(get_distribution)
  LSB_DIST="$(echo "$lsb_dist" | tr '[:upper:]' '[:lower:]')"
}

set_sh_c() {
  SH_C='sh -c'
  BASH_C='bash -c'
  SUDO_SH_C='sh -c'
  if [ "$user" != 'root' ]; then
    if command_exists sudo; then
      SUDO_SH_C='sudo -E sh -c'
    elif command_exists su; then
      SUDO_SH_C='su -c'
    else
      cat >&2 <<-'EOF'
Error: this installer needs the ability to run commands as root.
We are unable to find either "sudo" or "su" available to make this happen.
EOF
      exit 1
    fi
  fi

  if is_dry_run; then
    echo "# Dry run mode"
    SH_C="echo"
    BASH_C="echo"
    SUDO_SH_C="echo"
  fi
}

sh_c() {
  eval "$SH_C '$@'"
}

bash_c() {
  eval "$BASH_C '$@'"
}

sudo_sh_c() {
  eval "$SUDO_SH_C '$@'"
}

print_instructions() {
  printf "\n\n\n\n"
  echo "######################"
  echo "##### POST SETUP #####"
  echo "######################"
  printf "\n\n"

  echo "##### GIT #####"
  echo "Add your identity"
  echo "git config --global user.name \"Your Name\""
  echo "git config --global user.email \"you@example.com\""
  printf "\n\n"

  echo "##### TMUX #####"
  echo "Run \"tmux\", then to install the plugins: prefix + I (capital i, as in Install)"
  printf "\n\n"

  if command_exists nvim; then
    echo "##### NeoVim #####"
    echo "Run \"nvim -c 'PackerSync'\""
    printf "\n\n"
  fi
}

install_packages() {
  PACKAGES=""
  while [ $# -gt 0 ]; do
    if ! command_exists $1; then
      PACKAGES="$PACKAGES$1 "
    fi
    shift $(($# > 0 ? 1 : 0))
  done

  if [ -n "$PACKAGES" ]; then
    echo "# Installing $PACKAGES"
    case "$LSB_DIST" in
    ubuntu | debian | raspbian)
      sudo_sh_c apt install -y $PACKAGES
      ;;

    centos | fedora | rhel)
      sudo_sh_c yum install -y $PACKAGES
      ;;

    alpine)
      sudo_sh_c apk add $PACKAGES
      ;;

    *)
      if [ -z "$LSB_DIST" ]; then
        if is_darwin; then
          sh_c brew install $PACKAGES
        fi
      fi
      ;;
    esac
  fi
}

git_clone() {
  echo "Git Clone $1"
  if [ -d "$2" ]; then
    echo "$2 already exists"
  else
    sh_c git clone "$@"
  fi
}

install_oh_my_zsh() {
  echo "# Installing Oh my Zsh"
  if [ -d "$HOME/.oh-my-zsh" ]; then
    if command_exists upgrade_oh_my_zsh; then
      sh_c upgrade_oh_my_zsh
    fi
  else
    sh_c curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -o /tmp/install_oh_my_zsh.sh
    sh_c chmod +x /tmp/install_oh_my_zsh.sh
    sh_c RUNZSH=no /tmp/install_oh_my_zsh.sh
    sh_c rm -f /tmp/install_oh_my_zsh.sh
  fi
}

install_oh_my_zsh_packages() {
  echo "# Installing Oh my Zsh Packages"
  if command_exists peco; then
    # https://github.com/jimeh/zsh-peco-history
    git_clone https://github.com/jimeh/zsh-peco-history.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-peco-history
  fi
  # https://github.com/zsh-users/zsh-autosuggestions
  git_clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
  # https://github.com/zsh-users/zsh-syntax-highlighting
  git_clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
}

install_asdf() {
  sh_c ./scripts/install-asdf.sh
}

setup_git() {
  echo "# Setup Git"
  sh_c sed "s#__HOME__#${HOME}#g" configs/git/.gitconfig >~/.gitconfig
  sh_c cp configs/git/.gitignore_global ~/.gitignore_global
}

setup_zsh() {
  echo "# Setup Zsh"
  sh_c cp ~/.zshrc ~/.zshrc.bak
  PACKAGES="asdf dotenv git zsh-autosuggestions zsh-syntax-highlighting"
  if command_exists peco; then
    PACKAGES="$PACKAGES zsh-peco-history"
  fi
  if is_darwin; then
    PACKAGES="$PACKAGES macos"
  fi
  $SH_C "sed -i.bak -e 's/plugins=([^\)]*)/plugins=($PACKAGES)/' ~/.zshrc"
  $SH_C "sed -i.bak -e '/HIST_STAMPS=\"mm\/dd\/yyyy\"/ {' -e 'r configs/zsh/history' -e '}' ~/.zshrc"

  zshrc_file="$HOME/.zshrc"
  if grep -q "# TEMPLATE: START" ~/.zshrc; then
    echo ".zshrc is already setup. Adding the config to .zshrc.tmp"
    sh_c cat ~/.zshrc >~/.zshrc_tmp
    zshrc_file="$HOME/.zshrc_tmp"
  fi

  sh_c echo '"# TEMPLATE: START"' >>$zshrc_file
  sh_c cat configs/zsh/aliases >>$zshrc_file

  if [ -f "/opt/homebrew/bin/brew" ]; then
    sh_c echo '"eval \"\$(/opt/homebrew/bin/brew shellenv)\""' >>$zshrc_file
  fi

  sh_c cat configs/zsh/exports >>$zshrc_file
  sh_c echo '"# TEMPLATE: END"' >>$zshrc_file
}

setup_bin() {
  echo "# Setup Bin files"
  sh_c mkdir -p ~/.bin
  sh_c cp -r configs/bin/* ~/.bin/
}

setup_tmux() {
  echo "# Setup Tmux"
  # https://github.com/tmux-plugins/tpm
  git_clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
  sh_c cp configs/tmux/.tmux.conf ~/.tmux.conf
}

setup_nvim() {
  echo "# Setup NeoVim"
  if ! command_exists nvim; then
    echo "Neovim not found, skipping"
    return
  fi

  git_clone https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim --depth 1

  sh_c mkdir -p ~/.config/
  sh_c cp -r configs/nvim ~/.config/

  sh_c mkdir -p ~/.config/nvim/colors
  sh_c curl -LJ https://raw.githubusercontent.com/tomasr/molokai/master/colors/molokai.vim -o ~/.config/nvim/colors/molokai.vim
}

install_powerling_fonts() {
  echo "# Install Powerline Tmux"
  git_clone https://github.com/powerline/fonts.git /tmp/.powerline-fonts --depth=1
  $SH_C "(cd /tmp/.powerline-fonts && ./install.sh)"
  sh_c rm -rf /tmp/.powerline-fonts
}

do_install() {
  echo "# Executing setup install script"

  set_sh_c
  set_lsb_dist

  case "$LSB_DIST" in
  ubuntu | debian | raspbian)
    echo "# System detected: $LSB_DIST"
    echo "# Repo update"
    sudo_sh_c apt update

    export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true TZ=Etc/UTC

    install_packages curl wget bash zsh tmux git peco software-properties-common

    # Force nvim >= 0.5.0
    sudo_sh_c add-apt-repository ppa:neovim-ppa/stable -y
    sudo_sh_c apt-get update

    install_packages neovim
    install_packages dirmngr gpg gawk
    install_packages jq ripgrep fzf
    install_powerling_fonts
    ;;

  centos | fedora | rhel)
    echo "# System detected: $LSB_DIST"
    echo "# Repo update"
    sudo_sh_c yum update -y

    install_packages https://repo.ius.io/ius-release-el7.rpm \
      https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

    install_packages curl wget bash zsh git peco
    install_packages tmux2u
    install_packages gpg gawk which
    install_powerling_fonts
    ;;

  alpine)
    echo "# System detected: $LSB_DIST"
    echo "# Repo update"
    sudo_sh_c apk update

    install_packages curl wget bash zsh coreutils tmux git neovim
    install_packages dirmngr gpg gpg-agent gawk
    install_powerling_fonts
    ;;

  *)
    if [ -z "$LSB_DIST" ]; then
      if is_darwin; then
        echo "# System detected: darwin"

        # Setting up brew
        if ! command_exists brew; then
          $BASH_C "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
          eval "$(/opt/homebrew/bin/brew shellenv)"
        fi

        # xcode-select --install
        install_packages curl wget bash zsh coreutils tmux peco neovim
        install_packages gpg gawk
        install_packages jq rg fzf
        install_powerling_fonts
      fi
    fi
    ;;

  esac

  install_oh_my_zsh
  install_oh_my_zsh_packages
  install_asdf

  setup_git
  setup_zsh
  setup_bin
  setup_tmux
  setup_nvim
}

setup_color

do_install

print_instructions
